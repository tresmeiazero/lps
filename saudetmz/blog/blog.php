<!DOCTYPE html>
<html lang="en">
<head>
  <title>Planos de Saúde 360 | Blog</title>

    <?php include("../includes/includesHeader.php"); ?>
    <?php include("../includes/IncludesPixel.php"); ?>

</head>

<body>

  <!-- Preloader -->
  <div class="loader-mask">
    <div class="loader">
      "Loading..."
    </div>
  </div>

  <main class="main-wrapper">

    <!-- Navigation -->
      <header class="nav">
          <div class="nav__holder nav--sticky">
              <div class="container-fluid container-semi-fluid nav__container">
                  <div class="flex-parent">

                      <div class="nav__header">
                          <!-- Logo -->
                          <a href="../index.php" class="logo-container flex-child">
                              <img class="logo" src="../img/logo.png" srcset="../img/logo.png 1x, ../img/logo@2x.png 2x" alt="logo">
                          </a>

                          <!-- Mobile toggle -->
                          <button type="button" class="nav__icon-toggle" id="nav__icon-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                              <span class="sr-only">Toggle navigation</span>
                              <span class="nav__icon-toggle-bar"></span>
                              <span class="nav__icon-toggle-bar"></span>
                              <span class="nav__icon-toggle-bar"></span>
                          </button>
                      </div>

                      <!-- Navbar -->
                      <nav id="navbar-collapse" class="nav__wrap collapse navbar-collapse">
                          <ul class="nav__menu">
                              <li class="active">
                                  <a href="index.php">Home</a>
                              </li>
                              <li>
                                  <a href="#">Contato</a>
                              </li>
                          </ul> <!-- end menu -->
                      </nav> <!-- end nav-wrap -->

                      <div class="nav__btn-holder nav--align-right">
                          <a href="#" class="btn nav__btn botaoshake">
                              <span class="nav__btn-text">Entre em contato</span>
                              <span class="nav__btn-phone">Clique aqui</span>
                          </a>
                      </div>

                  </div> <!-- end flex-parent -->
              </div> <!-- end container -->

          </div>
      </header> <!-- end navigation -->

      <div class="content-wrapper oh">

      <!-- Page Title -->
      <section class="page-title text-center">
        <div class="container">
          <div class="page-title__holder">
            <h1 class="page-title__title">Novidades</h1>
            <p class="page-title__subtitle">Fique por dentro de todas novidades sobre Planos de Saúde.</p>
          </div>
        </div>
      </section> <!-- end page title -->

      <!-- Blog -->
      <section class="section-wrap bottom-divider">
        <div class="container">
            <div class="row card-row">

                <div class="col-lg-4">
                    <article class="entry card box-shadow hover-up">
                        <div class="entry__img-holder card__img-holder">
                            <a href="plano-de-saude-carencia-zero.php">
                                <img src="../img/blog/post_1.jpg" class="entry__img" alt="">
                            </a>
                            <div class="entry__date">
                                <span class="entry__date-day">11</span>
                                <span class="entry__date-month">jul</span>
                            </div>
                            <div class="entry__body card__body">
                                <h4 class="entry__title">
                                    <a href="plano-de-saude-carencia-zero.php">Plano de Saúde carência zero existe ou não?</a>
                                </h4>
                                <ul class="entry__meta">
                                    <li class="entry__meta-category">
                                        <i class="ui-category"></i>
                                        <a href="#">Planos de Saúde</a>
                                    </li>
                                    <li class="entry__meta-star">
                                        <i class="ui-star"></i>
                                        <a href="#">Leitura: 5 Min</a>
                                    </li>
                                </ul>
                                <div class="entry__excerpt">
                                    <p>Um dos maiores desejos dos brasileiros é ter um plano de saúde carência zero. Seja para tratar alguma doença, ou para simplesmente ter uma segurança para si ou para toda a família.</p>
                                </div><br>
                                <a href="plano-de-saude-carencia-zero.php">Saiba mais</a>
                            </div>
                        </div>
                    </article>
                </div>

                <div class="col-lg-4">
                    <article class="entry card box-shadow hover-up">
                        <div class="entry__img-holder card__img-holder">
                            <a href="como-fazer-um-plano-de-saude-empresarial.php">
                                <img src="../img/blog/post_2.jpg" class="entry__img" alt="">
                            </a>
                            <div class="entry__date">
                                <span class="entry__date-day">09</span>
                                <span class="entry__date-month">jul</span>
                            </div>
                            <div class="entry__body card__body">
                                <h4 class="entry__title">
                                    <a href="como-fazer-um-plano-de-saude-empresarial.php">Como fazer um Plano de Saúde Empresarial</a>
                                </h4>
                                <ul class="entry__meta">
                                    <li class="entry__meta-category">
                                        <i class="ui-category"></i>
                                        <a href="#">Planos de Saúde</a>
                                    </li>
                                    <li class="entry__meta-star">
                                        <i class="ui-star"></i>
                                        <a href="#">Leitura: 6 Min</a>
                                    </li>
                                </ul>
                                <div class="entry__excerpt">
                                    <p>O plano de saúde empresarial tem agradado a muitos usuários pelos seus muitos benefícios, mas você sabe como fazer um? A empresa que não quer dor de cabeça nem precisa pensar muito para saber que fazer um plano de saúde empresarial é a escolha certa.</p>
                                    <br>
                                    <a href="como-fazer-um-plano-de-saude-empresarial.php">Saiba mais</a>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>

                <div class="col-lg-4">
                    <article class="entry card box-shadow hover-up">
                        <div class="entry__img-holder card__img-holder">
                            <a href="../plano-de-saude-cobre-ou-nao-cirurgia-plastica.php">
                                <img src="../img/blog/post_3.jpg" class="entry__img" alt="">
                            </a>
                            <div class="entry__date">
                                <span class="entry__date-day">08</span>
                                <span class="entry__date-month">jul</span>
                            </div>
                            <div class="entry__body card__body">
                                <h4 class="entry__title">
                                    <a href="plano-de-saude-cobre-ou-nao-cirurgia-plastica.php">Plano de Saúde cobre ou não cirurgia plástica?</a>
                                </h4>
                                <ul class="entry__meta">
                                    <li class="entry__meta-category">
                                        <i class="ui-category"></i>
                                        <a href="#">Planos de Saúde</a>
                                    </li>
                                    <li class="entry__meta-star">
                                        <i class="ui-star"></i>
                                        <a href="#">Leitura: 6 Min</a>
                                    </li>
                                </ul>
                                <div class="entry__excerpt">
                                    <p>O Brasil é o segundo país do mundo em quantidade de cirurgia plástica, com mais de 1,22 milhão de procedimentos realizados, perdendo apenas para os Estados Unidos, que somam mais de 1,41 milhão.</p>
                                    <br>
                                    <a href="plano-de-saude-cobre-ou-nao-cirurgia-plastica.php">Saiba mais</a>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>

            </div>

          <!-- Pagination -->
          <nav class="pagination">
            <span class="pagination__page pagination__page--current">1</span>
            <!--<a href="#" class="pagination__page">2</a>-->
            <!--<a href="#" class="pagination__page">3</a>-->
            <!--<a href="#" class="pagination__page">4</a>-->
            <a href="#" class="pagination__page pagination__icon pagination__page--next"><i class="ui-arrow-right"></i></a>
          </nav>

        </div>
      </section> <!-- end blog -->


      <!-- Footer -->
      <footer class="footer">
        <div class="container">
          <div class="footer__widgets">
            <div class="row">

              <div class="col-lg-3 col-md-6">
                <div class="widget widget-about-us">
                  <!-- Logo -->
                  <a href="../index.php" class="logo-container flex-child">
                    <img class="logo" src="../img/logo.png" srcset="../img/logo.png 1x, ../img/logo@2x.png 2x" alt="logo">
                  </a>
                  <p class="mt-24 mb-32">Encontre o plano de saúde que mais se encaixa na sua necessidade.</p>
                  <div class="socials">
                    <a href="#" class="social social-twitter" aria-label="twitter" title="twitter" target="_blank"><i class="ui-twitter"></i></a>
                    <a href="#" class="social social-facebook" aria-label="facebook" title="facebook" target="_blank"><i class="ui-facebook"></i></a>
                    <a href="#" class="social social-google-plus" aria-label="google plus" title="google plus" target="_blank"><i class="ui-google"></i></a>
                  </div>
                </div>
              </div> <!-- end about us -->


              <div class="col-lg-2 offset-lg-5 col-md-6">
                <div class="widget widget_nav_menu">
                  <h5 class="widget-title">Saúde 360</h5>
                  <ul>
                    <li><a href="../index.php">Home</a></li>
                    <li><a href="../index.php#planos">Planos</a></li>
                    <li><a href="../index.php#sobrenos">Sobre</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="../index.php#">Contato</a></li>
                  </ul>
                </div>
              </div>



              <div class="col-lg-2 col-md-6">
                <div class="widget widget-address">
                  <h5 class="widget-title">Operadoras</h5>
                  <ul>
                    <li><a href="#">Amil</a></li>
                    <li><a href="#">Bradesco</a></li>
                    <li><a href="#">Sulamerica</a></li>
                    <li><a href="#">Intermedica</a></li>
                  </ul>
                </div>
              </div>

            </div>
          </div>
        </div> <!-- end container -->

        <div class="footer__bottom top-divider">
          <div class="container text-center">
            <span class="copyright">
              &copy; 2018 Saúde 360, Desenvolvido por <a href="https://agenciatresmeiazero.com.br">Agência TresMeiaZero</a>
            </span>
          </div>
        </div> <!-- end footer bottom -->
      </footer> <!-- end footer -->

      <div id="back-to-top">
        <a href="#top"><i class="ui-arrow-up"></i></a>
      </div>

    </div> <!-- end content wrapper -->
  </main> <!-- end main wrapper -->

  <?php include("../includes/includesFooter.php"); ?>

</body>
</html>
